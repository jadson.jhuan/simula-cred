simula-cred
Projeto para Simulação de Financiamento.

Front-end:
    Diretório finjjs. Desenvolvido em React JS, para consumir o serviço de back-end.
    Para executar basta executar o comando npm start na raiz do projeto.

Back-end:
    Diretório creditSimulator. Desenvolvido em Java(SpringBoot).
    Serviço para controle de cadastro de clientes e realizar simulação de financiamento.
    Obs. O cálculo utilizado para a simulação foi desenvoldido com base no Banco Central do Brasil.
    O projeto está sendo executado na porta 8081.
    Requisito para executar o projeot:
        Banco de dados MySQl (nome da base "db_cred", usuário "root", senha "");
    Foram criados dois testes unitários para o projeto.
        testClientInsert
        testClientRiskA
