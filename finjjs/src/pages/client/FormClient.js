import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

import Navbar from '../../Navbar';
import api from '../../services/api';

export default function ListClient({ match }) {

    const [client, setClient] = useState({
        name: '',
        salary: 0,
        risk: 'C'
    });
    const [address, setAddress] = useState({
        street: '',
        number: '',
        complement: '',
        city: '',
        state: ''
    });

    useEffect(() => {
        async function getClient() {
            const response = await api.get(`/client/${match.params.id}`);
            setClient(response.data);
            setAddress(response.data.address);
        }
        getClient();
    }, [match.params.id]);

    async function handleSubmit(event) {
        event.preventDefault();
        let clientTemp = client;
        clientTemp.address = address;
        if (clientTemp.salary <= 2000) {
            clientTemp.risk = "C";
        } else if (clientTemp.salary <= 8000) {
            clientTemp.risk = "B";
        } else {
            clientTemp.risk = "A";
        }
        let response;
        if (clientTemp.id) {
            response = await api.put("/client", clientTemp)
                .catch(e => console.log(e));
        } else {
            response = await api.post("/client", clientTemp)
                .catch(e => console.log(e));
        }

        alert('Registro gravado com sucesso!');
        window.location = "/";
    }

    const handleClientInputChange = event => {
        const { name, value } = event.target;
        setClient({ ...client, [name]: value });
    }
    const handleAddressInputChange = event => {
        const { name, value } = event.target;
        setAddress({ ...address, [name]: value });
    }

    return (
        <div className="container">
            <Navbar />
            <div className="content">
                <div className="titlePage">
                    <h1>Clientes / cadastro</h1>
                </div>
                <div className="content-data">
                    <form onSubmit={handleSubmit} method="POST">
                        <div className="formGroup">
                            <label>Nome</label>
                            <input
                                required
                                name="name"
                                placeholder="Dígite o nome do cliente"
                                value={client.name}
                                onChange={handleClientInputChange}
                            />
                        </div>
                        <div className="formGroup">
                            <label>Salário</label>
                            <input
                                required
                                name="salary"
                                type="number"
                                className="number"
                                placeholder="Dígite o salário"
                                value={client.salary}
                                onChange={handleClientInputChange}
                            />
                        </div>
                        <div className="titlePage">
                            <br />
                            <h3>Endereço</h3>
                        </div>
                        <div className="formGroup">
                            <label>CEP</label>
                            <input
                                name="zipCode"
                                placeholder="Dígite o CEP"
                                value={address.zipCode}
                                onChange={handleAddressInputChange}
                            />
                        </div>
                        <div className="formGroup">
                            <label>Rua</label>
                            <input
                                required
                                name="street"
                                placeholder="Dígite a rua"
                                value={address.street}
                                onChange={handleAddressInputChange}
                            />
                        </div>
                        <div className="formGroup">
                            <label>Nº</label>
                            <input
                                name="number"
                                placeholder="Dígite o Nº"
                                value={address.number}
                                onChange={handleAddressInputChange}
                            />
                        </div>
                        <div className="formGroup">
                            <label>Complemento</label>
                            <input
                                name="complement"
                                placeholder="Dígite a rua"
                                value={address.complement}
                                onChange={handleAddressInputChange}
                            />
                        </div>
                        <div className="formGroup">
                            <label>Cidade</label>
                            <input
                                required
                                name="city"
                                placeholder="Dígite a cidade"
                                value={address.city}
                                onChange={handleAddressInputChange}
                            />
                        </div>
                        <div className="formGroup">
                            <label>Estado</label>
                            <input
                                required
                                name="state"
                                placeholder="Dígite o estado"
                                value={address.state}
                                onChange={handleAddressInputChange}
                            />
                        </div>
                        <hr />
                        <div className="barForm">
                            <Link to="/">
                                <button type="button" className="btn btn-lg btn-default">
                                    VOLTAR
                                </button>
                            </Link>
                            &nbsp; &nbsp;
                            <button type="submit" className="btn btn-lg btn-success">
                                SALVAR
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    );
};