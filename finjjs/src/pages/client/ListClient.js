import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

import Navbar from '../../Navbar';
import api from '../../services/api';
import IconEdit from '../../assets/edit.png';
import IconDelete from '../../assets/delete.png';
import IconSimule from '../../assets/save-money.png';

export default function ListClient() {

    const [clients, setClients] = useState([]);
    const [toast, setToast] = useState({
        title: '',
        message: ''
    });

    useEffect(() => {
        async function loadClients() {
            const response = await api.get('/client', {});
            setClients(response.data);
        }
        loadClients();
    }, []);

    async function remove(client) {
        if (window.confirm(`Deseja confirmar a exclusão deste registro?\n [${client.id}] ${client.name}`)) {
            let response = await api.delete(`/client/${client.id}`);
            setToast({
                title: 'Sucesso',
                message: 'Registro removido com sucesso!'
            });
            setTimeout(() => setToast({ title: '', message: '' }), 3500);
            response = await api.get('/client', {});
            setClients(response.data);
        }
    }

    async function showMessage(title, message) {
        setTimeout(() => document.getElementById("toast").remove(), 3500);
    }

    return (
        <div className="container">
            <Navbar />
            {
                toast.message.length > 0 ?
                    <div className="toast" id="toast">
                        <span>{toast.title}</span>
                        <p>{toast.message}</p>
                    </div> : ''
            }

            <div className="content">
                <div className="titlePage">
                    <h1>
                        Clientes
                        <a href="/cadastro" className="btnDefault btnNewClient btn btn-lg btn-success">
                            + ADICIONAR
                        </a>
                    </h1>
                </div>
                <div className="content-data">
                    <table cellspacing="0">
                        <thead>
                            <tr>
                                <th>Nome</th>
                                <th className="col-actions">Salário</th>
                                <th className="col-actions textCenter">Risco</th>
                                <th>Endereço</th>
                                <th className="col-actions">&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            {clients.map(client => (
                                <tr key={client.id}>
                                    <td>{client.name}</td>
                                    <td>R$ {client.salary.toFixed(2)}</td>
                                    <td className="textCenter">{client.risk}</td>
                                    <td>{client.address.street}</td>
                                    <td>
                                        <button type="button" className="btn btn-default" title="Editar Cliente">
                                            <Link to={"/cadastro/" + client.id}>
                                                <img src={IconEdit} />
                                            </Link>
                                        </button>
                                        &nbsp;
                                        <button type="button" className="btn btn-default" title="Simular Empréstimo">
                                            <Link to={"/simulador/" + client.id}>
                                                <img src={IconSimule} />
                                            </Link>
                                        </button>
                                        &nbsp;
                                        <button onClick={() => remove(client)} type="button" className="btn btn-default" title="Excluir Registro">
                                            <img src={IconDelete} />
                                        </button>
                                    </td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    );
}