import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

import Navbar from '../../Navbar';
import api from '../../services/api';

import './Simulator.css';

import ImgLoading from '../../assets/loading.gif';
import IconSimuleWhite from '../../assets/money-16.png';

export default function Simulator({ match }) {

    const [progress, setProgress] = useState('');

    const [client, setClient] = useState({
        name: '',
        salary: 0,
        risk: 'C'
    });
    const [simulation, setSimulation] = useState({
        requestedValue: 0,
        split: 0,
        valueSplit: 0,
        valueInterest: 0,
        totalValueSimulation: 0,
        tax: 0,
    });

    useEffect(() => {
        async function getClient() {
            const response = await api.get(`/client/${match.params.id}`);
            setClient(response.data);
        }
        getClient();
    }, [match.params.id]);

    async function handleSubmit(event) {
        event.preventDefault();
        setSimulation({
            requestedValue: 0,
            split: 0,
            valueSplit: 0,
            valueInterest: 0,
            totalValueSimulation: 0,
            tax: 0,
        });
        setProgress("Enviando informações...");
        let time = Math.random() * (3 - 1.5) + 1.5;//RANDOM PARA SIMULAR PROCESSAMENTO

        const response = await api.post("/credit/simulate", {
            client: {
                id: client.id
            },
            requestedValue: simulation.requestedValue,
            split: simulation.split
        });

        //SIMULANDO PROCESSAMENTO DE CÁLCULOS, ANÁLISE DE CRÉDITO, ETC...
        setTimeout(() => {
            setProgress("Aplicando taxas...");
        }, (time / 3) * 1000);
        setTimeout(() => {
            setSimulation(response.data);
            setProgress('');
        }, time * 1000);
    }

    const handleInputChange = event => {
        const { name, value } = event.target;
        setSimulation({ ...simulation, [name]: value });
    }

    return (
        <div className="container">
            <Navbar />
            {
                progress.length > 0 ?
                    <div className="blockLoading">
                        <img src={ImgLoading} />
                        <p>{progress}</p>
                    </div>
                    : ''
            }

            <div className="content">
                <div className="titlePage">
                    <h1>Simulador de Crédito</h1>
                </div>
                <div className="content-data">
                    <div className="titlePage">
                        <br />
                        <h3>Cliente:</h3>
                        <h2>{client.name}</h2>
                        <p>Renda: R$ {client.salary.toFixed(2)}</p>
                        <p>Risco: {client.risk}</p>
                    </div>

                    <form onSubmit={handleSubmit}>
                        <div className="formGroup">
                            <label>Quanto você Precisa?</label>
                            <input
                                required
                                type="number"
                                name="requestedValue"
                                placeholder="100"
                                value={simulation.requestedValue}
                                onChange={handleInputChange}
                            />
                        </div>
                        <div className="formGroup">
                            <label>Nº de Parcelas</label>
                            <input
                                required
                                type="number"
                                className="number"
                                min="1"
                                max="60"
                                name="split"
                                placeholder="100"
                                value={simulation.split}
                                onChange={handleInputChange}
                            />
                        </div>
                        <div className="formGroup">
                            <label><br /></label>
                            <button type="submit" className="btn btn-lg btn-success">
                                <img src={IconSimuleWhite} /> Calcular
                        </button>
                        </div>
                    </form>
                    {simulation.totalValueSimulation > 0 ?
                        <table className="tableSimulation" cellSpacing="1">
                            <tbody>
                                <tr>
                                    <th className="title" colSpan="2">Simulação</th>
                                </tr>
                                <tr>
                                    <th>Valor Solicitado</th>
                                    <td>R$ {simulation.requestedValue.toFixed(2)}</td>
                                </tr>
                                <tr>
                                    <th>Nº de Parcelas</th>
                                    <td>{simulation.split}</td>
                                </tr>
                                <tr>
                                    <th>Taxa de Juros</th>
                                    <td>{simulation.tax}% a.m</td>
                                </tr>
                                <tr>
                                    <th>Valor Parcelas</th>
                                    <td>R$ {simulation.valueSplit.toFixed(2)}</td>
                                </tr>
                                <tr>
                                    <th>Valor Juros</th>
                                    <td>R$ {simulation.valueInterest.toFixed(2)}</td>
                                </tr>
                                <tr>
                                    <th>Total</th>
                                    <td>R$ {simulation.totalValueSimulation.toFixed(2)}</td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <td colSpan="2">
                                    Cálculo realizado com referência no Banco Central do Brasil.
                                    <br />
                                    <a href="https://www3.bcb.gov.br/CALCIDADAO/publico/exibirFormFinanciamentoPrestacoesFixas.do?method=exibirFormFinanciamentoPrestacoesFixas"
                                        target="_blank">
                                        <u>ver referência</u>
                                    </a>
                                </td>
                            </tfoot>
                        </table>
                        : ''}
                    <hr />
                    <div className="barForm">
                        <Link to="/">
                            <button type="button" className="btn btn-lg btn-default">
                                VOLTAR
                            </button>
                        </Link>
                    </div>
                </div>
            </div>
        </div>
    );

}