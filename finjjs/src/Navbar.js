import React from 'react';
import { Link } from 'react-router-dom';

import logo from './assets/logo.png'

export default function Navbar() {
    return (
        <nav className="navbar">
            <Link to="/">
                <img src={logo} alt="Simula Credi" className="logo" />
            </Link>
        </nav>
    );
}