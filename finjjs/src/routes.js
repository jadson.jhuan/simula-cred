import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';

import ListClient from './pages/client/ListClient';
import FormClient from './pages/client/FormClient';
import Simulator from './pages/credit/Simulator';

export default function Routes() {
    return (
        <BrowserRouter>
            <Route path="/" exact component={ListClient} />
            <Route path="/cadastro" exact component={FormClient} />
            <Route path="/cadastro/:id" exact component={FormClient} />
            <Route path="/simulador/:id" exact component={Simulator} />
        </BrowserRouter>
    );
}