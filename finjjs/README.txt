Projeto de front-end para consumir serviço em http://localhost:8081.

Executar comando "npm start".

Na tela principal é possívle visualizar a lista com os clientes cadastrados, podendo fazer inclusão, edição e remoção dos registros.

No grid de clientes, existe uma ação para realizar a simulação de empréstimo.

Simulação de crédito:
    Deve-se informar o valor desejado e o número de parcelas.
    O cálculo e realizado no serviço de back-end desenvolvido em Java, a fórmula utilizada foi tida como base o site do Banco Central do Brasil.
    Os detalhes dos valores serão exibidos em seguida.