package br.com.finjjs;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.finjjs.enumeration.Risk;
import br.com.finjjs.model.Address;
import br.com.finjjs.model.Client;
import br.com.finjjs.repository.ClientRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ClientRepositoryTest {

	@Autowired
	private ClientRepository clientRepository;
	
	@Test
	public void testClientInsert() {
		Client client = new Client();
		client.setName("Jadson Jhuan");
		client.setSalary(7500d);
		Address address = new Address();
		address.setStreet("Av. Torquato Tapajós");
		address.setCity("Manaus");
		address.setState("AM");
		client.setAddress(address);
		clientRepository.save(client);
		
		assertThat(client.getId()).isNotNull();		
	}
	
	@Test
	public void testClientRiskA() {
		Client client = new Client();
		client.setName("Jadson Jhuan");
		client.setSalary(8100d);
		Address address = new Address();
		address.setStreet("Av. Torquato Tapajós");
		address.setCity("Manaus");
		address.setState("AM");
		client.setAddress(address);
		clientRepository.save(client);
		assertThat(client.getRisk()).isEqualTo(Risk.A);
	}
	
}
