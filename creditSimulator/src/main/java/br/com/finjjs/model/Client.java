package br.com.finjjs.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import br.com.finjjs.enumeration.Risk;

@Entity
@Table(name = "client")
public class Client {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column
	@NotEmpty
	private String name;

	@Column
	@NotNull
	private Double salary;

	@Column
	@Enumerated(EnumType.STRING)
	private Risk risk;

	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL, optional = false)
	@JoinColumn(name="address_id")
	private Address address;

	public Long getId() {
		if(id == null) {
			id = new Long(0);
		}
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getSalary() {
		return salary;
	}

	public void setSalary(Double salary) {
		this.salary = salary;
	}

	public Risk getRisk() {
		if(salary <= 2000) {
			risk = Risk.C;
		} else if (salary > 2000 && salary <= 8000) {
			risk = Risk.B;
		} else if (salary > 8000) {
			risk = Risk.A;
		}
		return risk;
	}

	public void setRisk(Risk risk) {
		this.risk = risk;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

}
