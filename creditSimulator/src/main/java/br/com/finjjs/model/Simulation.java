package br.com.finjjs.model;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import br.com.finjjs.enumeration.Risk;

public class Simulation {

	private Client client;

	@Min(1)
	@Max(60)
	private Integer split;

	@Min(100)
	private Double requestedValue;

	private Double valueSplit;

	private Double valueInterest;

	private Double totalValueSimulation;

	private Double tax;

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public Integer getSplit() {
		return split;
	}

	public void setSplit(Integer split) {
		this.split = split;
	}

	public Double getRequestedValue() {
		return requestedValue;
	}

	public void setRequestedValue(Double requestedValue) {
		this.requestedValue = requestedValue;
	}

	public Double getValueInterest() {
		return valueInterest;
	}

	public void setValueInterest(Double valueInterest) {
		this.valueInterest = valueInterest;
	}

	public Double getTotalValueSimulation() {
		return totalValueSimulation;
	}

	public void setTotalValueSimulation(Double totalValueSimulation) {
		this.totalValueSimulation = totalValueSimulation;
	}

	public Double getValueSplit() {
		return valueSplit;
	}

	public void setValueSplit(Double valueSplit) {
		this.valueSplit = valueSplit;
	}

	public Double getTax() {
		if (client != null) {
			if (client.getRisk().equals(Risk.A)) {
				tax = 1.9;
			} else if (client.getRisk().equals(Risk.B)) {
				tax = 5d;
			} else if (client.getRisk().equals(Risk.C)) {
				tax = 10d;
			}
		}
		return tax;
	}

	public void setTax(Double tax) {
		this.tax = tax;
	}

	/**
	 * Método para simular o cálculo "complexo" para a simulação do crédito
	 * desejado.
	 * Obs.: O cálculo está sendo feito com base na fórmula disponibilizada pelo Banco Central do Brasil
	 */
	public void calculateInterest() {
		this.valueSplit = getRequestedValue() / ((Math.pow(1 + (getTax() / 100), getSplit()) - 1)
				/ (Math.pow(1 + (getTax() / 100), getSplit()) * (getTax() / 100)));
		
		this.totalValueSimulation = getValueSplit() * getSplit();
		this.valueInterest = getTotalValueSimulation() - getRequestedValue();
		
	}

}
