package br.com.finjjs.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.finjjs.enumeration.Risk;
import br.com.finjjs.model.Client;

public interface ClientRepository extends JpaRepository<Client, Long> {
	
	public Client findByNameAndRisk(String name, Risk risk);

}
