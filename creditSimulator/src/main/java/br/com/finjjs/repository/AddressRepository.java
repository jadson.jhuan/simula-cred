package br.com.finjjs.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.finjjs.model.Address;

public interface AddressRepository extends JpaRepository<Address, Long> {

}
