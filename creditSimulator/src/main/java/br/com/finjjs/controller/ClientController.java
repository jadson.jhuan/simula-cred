package br.com.finjjs.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.finjjs.exeption.AlreadyExistingModelException;
import br.com.finjjs.exeption.ModelNotFoundException;
import br.com.finjjs.model.Client;
import br.com.finjjs.repository.ClientRepository;

@RestController
@RequestMapping("/client")
@CrossOrigin("*")
public class ClientController {

	@Autowired
	private ClientRepository clientRepo;

	@GetMapping(produces = "application/json")
	public @ResponseBody List<Client> listAll() {
		return clientRepo.findAll();
	}

	@GetMapping(value = "/{id}")
	public ResponseEntity<Client> get(@PathVariable("id") Long id) {
		Optional<Client> clientOp = clientRepo.findById(id);
		if (!clientOp.isPresent()) {
			throw new ModelNotFoundException("Registro " + id + " não encontrado!");
		}

		return new ResponseEntity<>(clientRepo.findById(id).get(), HttpStatus.OK);
	}

	@PostMapping
	public ResponseEntity<Client> store(@RequestBody @Valid Client client) {
		Optional<Client> clientOp = clientRepo.findById(client.getId());
		if (clientOp.isPresent()) {
			throw new AlreadyExistingModelException("ID " + client.getId() + " já registrado anteriormente!");
		}
		return new ResponseEntity<>(clientRepo.save(client), HttpStatus.OK);
	}

	@PutMapping
	public ResponseEntity<Client> update(@RequestBody @Valid Client client) {
		Optional<Client> clientOp = clientRepo.findById(client.getId());
		if (!clientOp.isPresent()) {
			throw new ModelNotFoundException("Registro " + client.getId() + " não encontrado!");
		}
		return new ResponseEntity<>(clientRepo.save(client), HttpStatus.OK);
	}

	@DeleteMapping(value = "/{id}")
	public ResponseEntity<Client> delete(@PathVariable("id") Long id) {
		Optional<Client> clientOp = clientRepo.findById(id);
		if (clientOp.isPresent()) {
			Client client = clientOp.get();
			clientRepo.deleteById(id);
			return new ResponseEntity<>(client, HttpStatus.OK);
		} else {
			throw new ModelNotFoundException("Registro " + id + " não encontrado!");
		}
	}

}
