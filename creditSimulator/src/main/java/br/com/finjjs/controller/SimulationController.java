package br.com.finjjs.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.finjjs.exeption.ModelNotFoundException;
import br.com.finjjs.model.Client;
import br.com.finjjs.model.Simulation;
import br.com.finjjs.repository.ClientRepository;

@RestController
@RequestMapping("/credit")
@CrossOrigin("*")
public class SimulationController {

	@Autowired
	private ClientRepository clientRepo;

	@PostMapping(path = "/simulate")
	public ResponseEntity<Simulation> simulateCredit(@RequestBody Simulation simulation) {
		Optional<Client> clientOp = clientRepo.findById(simulation.getClient().getId());
		
		if (!clientOp.isPresent()) {
			throw new ModelNotFoundException("Cliente " + simulation.getClient().getId() + " não encontrado!");
		}

		Simulation simulation_ = new Simulation();
		simulation_.setClient(clientOp.get());
		simulation_.setSplit(simulation.getSplit());
		simulation_.setRequestedValue(simulation.getRequestedValue());

		simulation_.calculateInterest();// MÉTODO P/ REALIZAR UM CÁLCULO MAIS COMPLEXO, CASO NECESSÁRIO

		return new ResponseEntity<>(simulation_, HttpStatus.OK);
	}

}
